/*
  ==============================================================================

    Grain.h
    Created: 11 Apr 2021 8:51:46pm
    Author:  brady

  ==============================================================================
*/

#pragma once
#include <Bela.h>
#include <vector>

class Grain {
 public:
  Grain(std::vector<float> env, int duration, float pbRate, int startPos, int trigTs, float gain)
      : mEnv(env), duration(duration), pbRate(pbRate), startPos(startPos), trigTs(trigTs), gain(gain) {}
  ~Grain() {}

  float process(std::vector<float>& fileBuffer, float gain, int time);

  float getAmplitude(float timePerc);

  int duration;  // Grain duration in samples
  int startPos;  // Start position in file to play from in samples
  int trigTs;    // Timestamp when grain was triggered in samples
  float pbRate;  // Playback rate (1.0 being regular speed)
  float gain;    // Grain gain

 private:
  std::vector<float> mEnv;
};