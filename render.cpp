#include <Bela.h>
#include <algorithm>
#include <cmath>
#include <libraries/AudioFile/AudioFile.h>
#include "Grain.h"

static constexpr auto PIN_PIEZO = 0;
static constexpr auto PIN_FSR = 1;
static constexpr auto ENV_LUT_SIZE = 128;
static constexpr float MIN_GRAIN_RATE_RATIO = 0.25f;
static constexpr float MAX_GRAIN_RATE_RATIO = 1.0f;
static constexpr float MIN_GRAIN_DURATION = 0.1f;
static constexpr float MAX_GRAIN_DURATION = 1.0f;
static constexpr float MIN_PB_RATE = 1.0f;
static constexpr float MAX_PB_RATE = 1.1f;
static constexpr float DEFAULT_GRAIN_SHAPE = 0.5f;
static constexpr float DEFAULT_GRAIN_TILT = 0.5f;
static constexpr auto MAX_GRAINS = 20;  // Max grains active at once

int mSamplesPerSensorRead;
std::vector<Grain> mGrains;
float mSamplesTilGrain = -1;
long mTotalSamps;
bool mPressureLocked = false;

// LUT of the grain envelope
std::vector<float> mGrainEnvLUT;

//  File bookkeeping
std::vector<float> mFileBuffer;
int mFileStartSample, mFileEndSample;

//float mInputPiezo; // Piezo sensor input
float mInputFsr; // FSR sensor input

void initGrainEnvelope() {
  mGrainEnvLUT.clear();
  /* LUT divided into 3 parts

               1.0
              -----
     rampUp  /     \  rampDown
            /       \
  */
  float scaledShape = (DEFAULT_GRAIN_SHAPE * ENV_LUT_SIZE) / 2.0f;
  float scaledTilt = DEFAULT_GRAIN_TILT * ENV_LUT_SIZE;
  int rampUpEndSample = max(0.0f, scaledTilt - scaledShape);
  int rampDownStartSample = min((float)ENV_LUT_SIZE, scaledTilt + scaledShape);
  for (int i = 0; i < ENV_LUT_SIZE; i++) {
    if (i < rampUpEndSample) {
      mGrainEnvLUT.push_back((float)i / rampUpEndSample);
    }
    else if (i > rampDownStartSample) {
      mGrainEnvLUT.push_back(1.0f - (float)(i - rampDownStartSample) / (ENV_LUT_SIZE - rampDownStartSample));
    }
    else {
      mGrainEnvLUT.push_back(1.0f);
    }
  }
}

void initFile(float sampleRate) {
	// Load sample file
	mFileBuffer = AudioFileUtilities::loadMono("bongo.wav");
	
	// Very simple start/end calculations
	// Start: just the start for now
	mFileStartSample = 0;
	mFileEndSample = mFileBuffer.size() - 1;
	// End: find 0.2s of samples all less than 0.01
	int silentCount = 0;
	int numSilentThresh = sampleRate * 0.2f;
	for (int i = 0; i < mFileBuffer.size(); ++i) {
		if (std::abs(mFileBuffer[i]) > 0.005f) {
			silentCount = 0;
		} else {
			silentCount++;
			if (silentCount >= numSilentThresh) {
				mFileEndSample = i  - numSilentThresh;
				break;
			}
		}
	}
	rt_printf("end sample of file: %f\n", (float)mFileEndSample / mFileBuffer.size());
	mFileBuffer.erase(mFileBuffer.begin() + mFileEndSample, mFileBuffer.end());
}

void checkAddGrains(BelaContext *context) {
	if (mInputFsr < 0.4) mPressureLocked = false;
	int durSamples = context->audioSampleRate * map(mInputFsr, 0.0f, 1.0f, MIN_GRAIN_DURATION, MAX_GRAIN_DURATION);
	durSamples = min(durSamples, mFileBuffer.size());
	int newSamplesTilGrain = durSamples * map(mInputFsr, 0.0f, 1.0f, MIN_GRAIN_RATE_RATIO, MAX_GRAIN_RATE_RATIO);
	if (newSamplesTilGrain < mSamplesTilGrain) mSamplesTilGrain = newSamplesTilGrain;
	
	if (mSamplesTilGrain <= 0 && mInputFsr < 0.95f && mInputFsr > 0.005f && !mPressureLocked) {
		if (mGrains.size() < MAX_GRAINS) {
			// TODO: clean this hardcoded shit up
    		int posSamples = map(1.0f - mInputFsr, 0.0f, 1.0f, 0, mFileBuffer.size() - durSamples);
    		float pbRate = map(mInputFsr, 0.0f, 1.0f, MIN_PB_RATE, MAX_PB_RATE);
			mGrains.push_back(Grain(mGrainEnvLUT, durSamples, pbRate, posSamples,
    			mTotalSamps, 0.7f/*min(0.2f, mInputFsr)*/));
    		if (mInputFsr > 0.6) mPressureLocked = true;	
		}
		mSamplesTilGrain = newSamplesTilGrain;
	}
}

bool setup(BelaContext *context, void *userData)
{
	initGrainEnvelope();
	
	// Check if analog channels are enabled
	if(context->analogFrames == 0 || context->analogFrames > context->audioFrames) {
		rt_printf("Error: this example needs analog enabled, with 4 or 8 channels\n");
		return false;
	}
	
	initFile(context->audioSampleRate);
	
	mTotalSamps = 0;
	
	if (context->analogFrames)
		mSamplesPerSensorRead = context->audioFrames / context->analogFrames;
	return true;
}

void render(BelaContext *context, void *userData)
{
	for (int i = 0; i < context->audioFrames; ++i) {
		
		// Sensor reading
		if(mSamplesPerSensorRead && !(i % mSamplesPerSensorRead)) {
			mInputFsr = constrain(analogRead(context, i / mSamplesPerSensorRead, PIN_FSR), 0.0f, 1.0f);
			checkAddGrains(context);
		}
		float output = 0.0f;
		for (int g = 0; g < mGrains.size(); ++g) {
			output += mGrains[g].process(mFileBuffer, 1.0f, mTotalSamps);
		}
		for(unsigned int channel = 0; channel < context->audioOutChannels; channel++) {
			audioWrite(context, i, channel, output * 0.5f);
		}
		mTotalSamps++;
		mSamplesTilGrain--;
	}
	
	// Remove expired grains
	mGrains.erase(std::remove_if(mGrains.begin(), mGrains.end(),
                              [](Grain g){ return mTotalSamps > (g.trigTs + g.duration); }), mGrains.end());
	
	// Reset timestamps if no grains active to keep numbers low
	if (mGrains.size() == 0) {
    	mTotalSamps = 0;
	}
}

void cleanup(BelaContext *context, void *userData)
{

}