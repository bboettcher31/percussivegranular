/*
  ==============================================================================

    Grain.cpp
    Created: 11 Apr 2021 8:51:46pm
    Author:  brady

  ==============================================================================
*/

#include "Grain.h"
#include <cmath>

float Grain::process(std::vector<float>& fileBuffer, float gain, int time) {
  if (startPos < 0) return 0.0f;
  float timePerc = (time - trigTs) / (float)duration;
  if (timePerc <= 0.0f || timePerc >= 1.0f) return 0.0f;
  float totalGain = gain * getAmplitude(timePerc);

  float unStretchedDuration = duration * pbRate;
  int lowSample = std::floor(max(0.0f, timePerc * unStretchedDuration));
  int highSample = std::ceil(max(0.0f, timePerc * unStretchedDuration));
  if (highSample >= fileBuffer.size()) return 0.0f;
  float rem = (timePerc * unStretchedDuration) - lowSample;

  float sample = map(rem, 0.0f, 1.0f, fileBuffer[(startPos + lowSample) % fileBuffer.size()],
                              fileBuffer[(startPos + highSample) % fileBuffer.size()]);
  sample *= totalGain;
  return sample;
}

float Grain::getAmplitude(float timePerc) {
  if (mEnv.size() == 0) return 0.0f;
  timePerc = constrain(timePerc, 0.0f, 1.0f);
  int i = timePerc * (mEnv.size() - 1);
  if (i < 0 || i > mEnv.size() - 1) return 0.0f;
  return mEnv[i];
}